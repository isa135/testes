# testes

## Getting started

```
npm init -y
```

```
npm install typescript -D
```

```
npm install jest -D
```

```
npm install ts-jest -D
```

```
npm install @types/jest -D
```

Cria arquivo jest.config.js
```
npx ts-jest config:init
```

Exemplo de arquivo jest.config.js
```
/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
};
```

```
npx jest
```

Exemplo de execução do comando acima:
```
No tests found, exiting with code 1
Run with `--passWithNoTests` to exit with code 0
In /Users/isa/git/testes
  3 files checked.
  testMatch: **/__tests__/**/*.[jt]s?(x), **/?(*.)+(spec|test).[tj]s?(x) - 0 matches
  testPathIgnorePatterns: /node_modules/ - 3 matches
  testRegex:  - 0 matches
Pattern:  - 0 matches
```

Instalar babel
```
npm install --save-dev babel-jest @babel/core @babel/preset-env
```

Criar o arquivo babel.config.js, conforme abaixo:

```
module.exports = {
  presets: [
    ['@babel/preset-env', {targets: {node: 'current'}}],
    '@babel/preset-typescript',
  ],
};
```

```
npm install --save-dev @babel/preset-typescript
```

Atualizar o arquivo babel.config.js


```
npm install --save-dev @jest/globals
```

Criar diretório
```
__test__
```

Criar arquivos nome.test.ts

Criar script para rodar o teste
"jest --coverage"

Com esses passos, consegui criar o arquivo "index" do coverage com o percentual funcionando.

***

## Links que usei para fazer o teste

- [ ] [Jest - Getting Started](https://jestjs.io/docs/getting-started#using-typescript)

- [ ] [Typescript e Jest - Escrevendo testes unitários em um projeto Node.js](https://dev.to/jhonywalkeer/typescript-e-jest-escrevendo-testes-unitarios-em-um-projeto-nodejs-d52)
